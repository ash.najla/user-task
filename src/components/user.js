import React from 'react';
import { Table, Divider, Button, Modal } from 'antd';
import { connect } from 'react-redux';
import 'antd/dist/antd.css';


class User extends React.Component {
  state = {
    visible: false, loading: false,
    columns: [
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        render: text => <span>{text}</span>,
      },
      {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
        render: text => <span>{text}</span>,
      },

      {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
          <span>
            <a>edit</a>
            <Divider type="vertical" />
            <a className={`${this.props.className}-delete`} onClick={(e) => { this.onDelete(record, e); }} > Delete </a>
          </span>
        ),
      },
    ]
  }

  //show model
  showModal = () => {
    this.setState({
      visible: true
    });
  };

  //delete function
  onDelete = (data) => {
    this.props.dispatch({ type: 'DELETE_POST', id: data.id })
  }

  //edit function
  onEdit = (data) => {
    this.setState({ editvisible: true })
    // this.props.dispatch({ type: 'DELETE_POST', id: data.id })
  }

  //cancel
  handleCancel = e => {
    this.setState({
      visible: false,
    });
  };

  //submit function
  handleOk = (e) => {
    var $this = this
    this.setState({ loading: true });
    setTimeout(function () {
      $this.setState({ loading: false });
      e.preventDefault();
      const name = $this.getName.value;
      const email = $this.getEmail.value;
      const data = {
        id: new Date(),
        name,
        email,
      }
      $this.props.dispatch({
        type: 'ADD_POST',
        data
      });
      $this.getName.value = '';
      $this.setState({
        visible: false,
      });

    }, 1000);
  }



  render() {
    return (
      <div className="App">
        <Button type="primary" className="my-5" onClick={this.showModal}>
          CREATE USER
        </Button>
        <Modal
          title="Create User"
          visible={this.state.visible}
          footer={null}
        >
          <form onSubmit={this.handleSubmit}>
            <input required className="mx-2" type="text" ref={(input) => this.getName = input} placeholder="Name" />
            <input required type="text" ref={(input) => this.getEmail = input} placeholder="Email" />
            <br /><br />
            <Button type="primary" onClick={this.handleOk} loading={this.state.loading}>Save </Button>
          </form>
        </Modal>
        <Table columns={this.state.columns} dataSource={this.props.posts} />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    posts: state
  }
}
export default connect(mapStateToProps)(User);